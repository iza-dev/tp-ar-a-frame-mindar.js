AFRAME.registerComponent("cloak", {
  init() {
    this.el.sceneEl.renderer.sortObjects = true;
    this.el.addEventListener("model-loaded", (e) => {
      const mesh = this.el.getObject3D("mesh");
      mesh.traverse((node) => {
        if (node.isMesh) {
          if (node.material) {
            node.material.colorWrite = false;
            node.renderOrder = 2;
          }
        }
      });
    });
  },
});

AFRAME.registerComponent("sphere", {
  init() {
    const mesh = this.el.getObject3D("mesh");
    mesh.traverse((node) => {
      if (node.isMesh) {
        if (node.material) {
          node.renderOrder = 3;
        }
      }
    });
  },
});

// AFRAME.registerComponent('gui', {
// init() {
//     const {el} = this
//     console.log('sepia')
//     el.addEventListener('click', (e) => {console.log('click')})
// },
// })

/*AFRAME.registerComponent('mysound', {
  init() {
    console.log('audio')
    const {el} = this
    el.components.sound.playSound()
  },
})*/
